import http
import logging
from datetime import datetime, timedelta, timezone
from typing import Any

from flask import Flask, Response, request, json, Request

from load_shedding import get_areas, get_area_schedule, get_stage, Provider
from load_shedding.providers import Area, ProviderError, Province, StageError, Stage

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("../load_shedding.log"),
        logging.StreamHandler()
    ]
)
logger = logging.getLogger(__name__)

app = Flask(__name__)


@app.route('/')
def main():
    index = "Examples<br/>" \
            "<a href='/providers'>/providers</a><br/>" \
            "<a href='/stage'>/stage</a><br/>" \
            "<a href='/schedule?provider_id=1&search_text=Milnerton'>/schedule?provider_id=1&search_text=Milnerton</a><br/>" \
            "<a href='/schedule?provider_id=1&suburb_id=1058852&province_id=9&stage=2'>/schedule?provider_id=1&suburb_id=1058852&province_id=9&stage=2</a><br/>" \
            "<a href='/schedule?provider_id=2&search_text=Edgemead'>/schedule?provider_id=2&search_text=Edgemead</a><br/>" \
            "<a href='/schedule?provider_id=2&area_id=13&province_id=9&stage=2'>/schedule?provider_id=2&area_id=13&province_id=9&stage=2</a><br/>"
    return Response(index, status=http.HTTPStatus.BAD_REQUEST)


@app.route('/providers')
def providers_handler():
    providers = {p.value: str(p) for p in list(Provider)}
    return Response(json.dumps(providers), mimetype='application/json')

    return Response(json.dumps("Unable to get stage"), status=http.HTTPStatus.NOT_FOUND, mimetype='application/json')


@app.route('/stage')
def stage_handler():
    provider_stages = {}
    for p in list(Provider):
        provider = p()
        stage = get_stage(provider)
        provider_stages[provider.name] = str(stage)

    return Response(json.dumps(provider_stages), mimetype='application/json')

    return Response(json.dumps("Unable to get stage"), status=http.HTTPStatus.NOT_FOUND, mimetype='application/json')


@app.route('/schedule')
def schedule_handler():
    provider = Provider(request.args.get("provider_id", type=int))()
    if not provider:
        return Response(json.dumps("Bad Request"), status=http.HTTPStatus.BAD_REQUEST, mimetype='application/json')

    res = search_handler(provider, request)
    if res:
        return res

    res = schedule_handler(provider, request)
    if res:
        return res

    return Response(json.dumps("Bad Request"), status=http.HTTPStatus.BAD_REQUEST, mimetype='application/json')


def schedule_handler(provider: Provider, req: Request) -> Any:
    province_id = req.args.get("province_id", type=int)
    area_id = req.args.get("suburb_id", req.args.get("area_id"))
    stage = req.args.get("stage", type=int)
    if not (province_id and area_id and stage):
        return None

    try:
        area = Area(id=area_id, province=Province(province_id))
        stage = Stage(stage)

        logging.log(logging.INFO, "Getting info for area: {area_id}".format(area_id=area.id))
        schedule = get_area_schedule(provider, area=area, stage=stage)
        logging.log(logging.INFO, "Schedule for {area_id} {stage}: {schedule}".format(
            area_id=area.id,
            stage=stage,
            schedule=schedule
        ))

        tz = timezone.utc
        days = 7
        forecast = []
        for s in schedule:
            start = s[0]
            end = s[1]
            if start.date() > datetime.now(tz).date() + timedelta(days=days):
                continue
            if end < datetime.now(tz):
                continue
            forecast.append({"start": start.isoformat(), "end": end.isoformat()})
    except (ProviderError, StageError) as e:
        return None
    else:
        return Response(json.dumps(forecast), mimetype='application/json')


def search_handler(provider: Provider, req: Request) -> Any:
    search_text = req.args.get("search_text")
    if not search_text:
        return None

    try:
        logging.log(logging.INFO, f"Searching for: {search_text}")
        areas = get_areas(provider, search_text=search_text)

        logging.log(logging.INFO, f"Found: {areas}".format(
            areas=",".join([str(area) for area in areas])
        ))

        schedule = get_area_schedule(provider, area=areas[0], stage=Stage.STAGE_4)
        logging.log(logging.INFO, "Schedule for {area} ({area_id}): {schedule}".format(
            area=search_text,
            area_id=areas[0].id,
            schedule=schedule
        ))
    except ProviderError as e:
        return Response(json.dumps(f"{e}"), status=http.HTTPStatus.INTERNAL_SERVER_ERROR, mimetype='application/json')
    else:
        return Response(json.dumps(schedule), mimetype='application/json')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)
