#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.
from datetime import timezone
from typing import List, Tuple

from load_shedding.providers import Stage, dict_list_to_obj_list
from load_shedding.providers.eskom import Area, Eskom, stage_from_status, filter_empty_areas
from tests.libs import mock_eskom, mock_citypower


class Eskom(Eskom):
    name = "Mock: Eskom Direct"

    def __init__(self) -> object:
        self.api = mock_eskom.LoadShedding()

    def get_areas(self, search_text: str = None, max_results: int = None) -> List[Area]:
        data = self.api.find_suburbs(search_text, max_results)
        areas = dict_list_to_obj_list(data, Area)
        return filter_empty_areas(areas)

    def get_area_schedule(self, area: Area, stage: Stage) -> List[Tuple]:
        data = self.api.get_schedule(province=area.province.value, suburb_id=area.id, stage=stage.value)
        return data

    def get_stage(self) -> Stage:
        data = self.api.get_status()
        return stage_from_status(data)

    def get_stage_forecast(self) -> List:
        """ Get Stage forecast from City Power."""
        stage_forecast =  mock_citypower.get_stage_forecast()
        for i in range(len(stage_forecast)):
            stage_forecast[i]["stage"] = Stage(stage_forecast[i].get("stage"))
            stage_forecast[i]["start_time"] = stage_forecast[i]["start_time"].astimezone(timezone.utc)
            stage_forecast[i]["end_time"] = stage_forecast[i]["end_time"].astimezone(timezone.utc)
        return stage_forecast
